Source: hipsolver
Section: devel
Homepage: https://github.com/ROCm/hipSOLVER
Priority: optional
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/rocm-team/hipsolver.git
Vcs-Browser: https://salsa.debian.org/rocm-team/hipsolver
Maintainer: Debian ROCm Team <debian-ai@lists.debian.org>
Uploaders: Maxime Chambonnet <maxzor@maxzor.eu>,
           Cordell Bloor <cgmb@slerp.xyz>,
           Christian Kastner <ckk@debian.org>,
Build-Depends: debhelper-compat (= 13),
               cmake,
               rocm-cmake,
               gfortran,
               rocm-device-libs-17,
               librocblas-dev,
               librocsolver-dev,
               liblapack-dev <!nocheck>,
               libgtest-dev <!nocheck>
Build-Depends-Indep: dh-sequence-sphinxdoc <!nodoc>,
               doxygen <!nodoc>,
               python3-breathe <!nodoc>,
               python3-sphinx <!nodoc>,
               python3-sphinx-rtd-theme <!nodoc>,
               libjs-jquery <!nodoc>,
               libjs-mathjax <!nodoc>,
               libjs-sphinxdoc <!nodoc>,
               libjs-underscore <!nodoc>
Rules-Requires-Root: no

Package: libhipsolver0
Section: libs
Architecture: amd64 arm64 ppc64el
Depends: ${misc:Depends}, ${shlibs:Depends},
Description: portable interface for GPU LAPACK routines - library
 hipSOLVER is a wrapper library that provides a common interface to rocSOLVER
 and cuSOLVER. The hipSOLVER library is designed to help applications using
 cuSOLVER to port their code to the ROCm platform.
 .
 This package provides the AMD ROCm hipSOLVER library.

Package: libhipsolver-dev
Section: libdevel
Architecture: amd64 arm64 ppc64el
Depends: libhipsolver0 (= ${binary:Version}),${misc:Depends}, ${shlibs:Depends},
         libamdhip64-dev
Suggests: libhipsolver-doc
Description: portable interface for GPU LAPACK routines - headers
 hipSOLVER is a wrapper library that provides a common interface to rocSOLVER
 and cuSOLVER. The hipSOLVER library is designed to help applications using
 cuSOLVER to port their code to the ROCm platform.
 .
 This package provides the AMD ROCm hipSOLVER development headers.

Package: libhipsolver0-tests
Section: libdevel
Architecture: amd64 arm64 ppc64el
Depends: libhipsolver0 (= ${binary:Version}),${misc:Depends}, ${shlibs:Depends},
Build-Profiles: <!nocheck>
Description: portable interface for GPU LAPACK routines - tests
 hipSOLVER is a wrapper library that provides a common interface to rocSOLVER
 and cuSOLVER. The hipSOLVER library is designed to help applications using
 cuSOLVER to port their code to the ROCm platform.
 .
 This package provides the AMD ROCm hipSOLVER test binaries used for verifying
 that the library is functioning correctly.

Package: libhipsolver-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Build-Profiles: <!nodoc>
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
         libjs-mathjax,
Breaks: libhipsolver-dev (<< 5.5.1-4~),
Replaces: libhipsolver-dev (<< 5.5.1-4~),
Description: portable interface for GPU LAPACK routines - documentation
 hipSOLVER is a wrapper library that provides a common interface to rocSOLVER
 and cuSOLVER. The hipSOLVER library is designed to help applications using
 cuSOLVER to port their code to the ROCm platform.
 .
 This package provides the AMD ROCm hipSOLVER documentation.
